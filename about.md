---
layout: article
title: 关于我
mathjax: false
pageview: false
---

<div id="ID_PHOTO" style="width: 18%;">
	<div class="card">
		<div class="card__image">
			<img class="image" src="https://img.be-my-only.xyz/about-me.jpg" />
		</div>
	</div>
</div>

### 教育经历

- 2016年09月&#126;: [华中科技大学](http://www.hust.edu.cn/)[电子信息与通信学院](http://eic.hust.edu.cn/) \| 博士在读
- 2015年09月&#126;2016年06月: 华中科技大学电子信息与通信学院 \| 硕士
- 2011年09月&#126;2015年06月: 华中科技大学电子信息与通信学院 \| 本科

### 科研经历

- 2018年10月&#126;2019年11月: [南洋理工大学计算机科学与工程学院](http://scse.ntu.edu.sg) 访学 \| 联合学习, 强化学习
- 2017年03月&#126;2018年08月: [中国科学院深圳先进技术研究院](http://www.siat.ac.cn/) 客座学生 \| 无线携能通信
- 2014年01月&#126;2017年02月: [华中科技大学智能互联网技术湖北省重点实验室](http://itec.hust.edu.cn/) \| 网络测量, 内容中心网

### 发表文章

<div align="justify" markdown="1">

- Jiawen Kang, Zehui Xiong, Dusit Niyato, **Yuze Zou**, Yang Zhang, and Mohsen Guizani, "Reliable federated learning for mobile networks", *IEEE Wireless Communications*, accepted. 
- Jing Xu, Shimin Gong, **Yuze Zou**, et al. “Redundant Sniffer Deployment for Multi-Channel Wireless Network Forensics with Unreliable Conditions,” *IEEE Transactions on Cognitive Communications and Networking*, accepted.  
- Jing Xu, **Yuze Zou**, Shimin Gong, et al. “Robust Transmissions in Wireless Powered Multi-Relay Networks with Chance Interference Constraints,” *IEEE Transactions on Communications*, 2018.
- **Yuze Zou**, Jing Xu, Shimin Gong, et al. “Backscatter-Aided Hybrid Data Offloading for Wireless Powered Edge Sensor Networks,” in *Proc. Globecom*, Hawaii, 2019. [\[slides\]](assets/slides/GC'19-hybrid-relay.pdf)[\[backup\]](https://res.be-my-only.xyz/GC'19-hybrid-relay.pdf)    
- **Yuze Zou**, Shaohan Feng, Dusit Niyato, et al. “Dynamic Games in Federated Learning Training Service Market,” in *Proc. PacRim*, Victoria, B.C., 2019. [\[slides\]](assets/slides/PacRim'19-dynamic-games.pdf)[\[backup\]](https://res.be-my-only.xyz/PacRim'19-dynamic-games.pdf)  
- **Yuze Zou**, Shaohan Feng, Dusit Niyato, et al. “Mobile Device Training Strategies in Federated Learning: An Evolutionary Game Approach,” in *Proc. GreenCom*, 2019. [\[slides\]](assets/slides/GreenCom'19-evolutionary-game.pdf)[\[backup\]](https://res.be-my-only.xyz/GreenCom'19-evolutionary-game.pdf)  
- Xi Luo, Jing Xu, **Yuze Zou**, et al. “Collaborative Relay Beamforming with Direct Links in Wireless Powered Communications,” in *Proc. WCNC*, Morocco, 2019.  
- **Yuze Zou**, Wei Liu, Shimin Gong, et al. “Joint Optimization of Wireless Power Transfer and Collaborative Beamforming for Relay Communications,” in *Proc. Globecom*, Abu Dhabi, 2018. [\[slides\]](assets/slides/GC'18-workshop-ps-relay.pdf)[\[backup\]](https://res.be-my-only.xyz/GC'18-workshop-ps-relay.pdf)  
- Ge Zhang, Wei Liu, **Yuze Zou**, Wenqing Cheng, “Simulator for Loosely-coupled Hybrid CDN-P2P Streaming Systems,” in *Proc. ICCSN*, Guangzhou, 2017.  
- Wei Liu, Ge Zhang, **Yuze Zou**, et al. “In-router cache performance under long-term video request traffic in CCN: A simulation-based study,” in *Proc. ICCSN*, Guangzhou, 2017.  
- **Yuze Zou**, Ge Zhang, Wei Liu, et al. “Effect of Caching Threshold in Content Centric Network: A Simulation-based Study,” in *Proc. ICCC*, Chengdu, 2016.  
- **Yuze Zou**, Wei Liu, Yang Yang, et al. “Energy-aware probabilistic forwarding in wireless content-centric networks,” in *Proc. ICTC*, Jesu, 2016.  
- Wei Liu, Ge Zhang, Jun Chen, **Yuze Zou**, and Wenchao Ding. "A measurement-based study on application popularity in Android and iOS app stores," in *Proc. Workshop on Mobile Big Data*, Hangzhou, 2015.  

</div>

### 专业技能

- 计算机程序语言
	- **掌握**: C++, MATLAB/cvx, Python/Tensorflow
	- **了解**: Java/Android
- 数学工具: 凸优化, 博弈论, 最优控制
- 计算机网络, TCP/IP协议

### 获得奖项

- 美国大学生数学建模比赛二等奖（Honorable Mention MCM/ICM, 2014）  
- 华中科技大学启明学院优秀毕业生，2015年

### *Love of my life*

![marry](https://img.be-my-only.xyz/marriage.jfif){: .shadow}


<style type="text/css">
#ID_PHOTO {
    position: relative;
    top: 10px;
    float: right;
    z-index: 99;
}
@media screen and (max-width: 800px) {
	#ID_PHOTO {
    	display: none !important;
	}
}
</style>

<script type="text/javascript">
(function () {
	document.querySelector(".article__header header").appendChild(
		document.querySelector("#ID_PHOTO")
	);
})();
</script>